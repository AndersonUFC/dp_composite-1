public class Operador implements Termo{

	char op;
	Termo t1,t2;

	// *** Constructor *** //
	public Operador(Termo t1, char op, Termo t2){
		this.t1 = t1;
		this.op = op;
		this.t2 = t2;
	}//Constructor

	// *** Termo implementation *** //
	public int evaluate() throws IllegalOperatorException{
		switch(op){
			case '+':
				return t1.evaluate() + t2.evaluate();
			case '-':
				return t1.evaluate() - t2.evaluate();
			case '*':
				return t1.evaluate() * t2.evaluate();
			default:
				throw new IllegalOperatorException();

		}//switch
	}//evaluate

}