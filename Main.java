public class Main{
	public static void main(String[] args){

		// Test: (3 + 4)*(5 + 6)
		Operando o1, o2, o3, o4;
		Operador op1, op2, op3;

		o1 = new Operando(3);
		o2 = new Operando(4);
		op1 = new Operador(o1, '+', o2);

		o3 = new Operando(5);
		o4 = new Operando(6);
		op2 = new Operador(o3, '+', o4);

		op3 = new Operador(op1, '*', op2);

		try{
			System.out.println(op3.evaluate());
		}catch(IllegalOperatorException e){
			System.out.println(e.toString());
		}
	}
}