/*
	Termo uses Composite

	parent → Termo
	childs → Operador, Operando
*/

public interface Termo{
	public int evaluate() throws IllegalOperatorException;
}//Termo