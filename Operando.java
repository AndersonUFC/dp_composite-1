public class Operando implements Termo{
	public int valor;

	// *** Constructor *** //
	public Operando(int valor){
		this.valor = valor;
	}//Constructor

	// *** Termo Implementation *** //
	public int evaluate() throws IllegalOperatorException{
		return valor;
	}//Evaluate
}